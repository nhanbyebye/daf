package com.nab.daf.audit.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class UserActivity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String ip;
	private String url;
	
	@Column(name = "request", columnDefinition = "LONGTEXT")
	private String request;

	@Column(name = "response", columnDefinition = "LONGTEXT")
	private String response;
	
	private long executionTime;
	private LocalDateTime accessTime;

}
