package com.nab.daf.audit.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nab.daf.audit.entity.UserActivity;
import com.nab.daf.audit.repository.UserActivityRepository;
import com.nab.daf.common.model.UserActivityModel;

@Service
public class AuditService {
	
	@Autowired
	private UserActivityRepository userActivityRepo;

	public void saveUserActivity(UserActivityModel uam) {
		UserActivity userActivity = new UserActivity();
		BeanUtils.copyProperties(uam, userActivity);
		userActivityRepo.save(userActivity);
	}

}
