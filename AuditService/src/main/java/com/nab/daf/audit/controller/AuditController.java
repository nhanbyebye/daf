package com.nab.daf.audit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nab.daf.audit.service.AuditService;
import com.nab.daf.common.model.Response;
import com.nab.daf.common.model.UserActivityModel;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/v1/audit", produces = "application/json")
@Api("Audit Services - track user activity")
public class AuditController {

	@Autowired
	private AuditService auditService;

	@PostMapping(path = "/write")
	@ApiOperation("To store user activity to DB")
	public Response<Void> trackUserActivity(@RequestBody UserActivityModel uam) {
		auditService.saveUserActivity(uam);
		return new Response<>();
	}

}
