package com.nab.daf.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.nab.daf.common.communication.AuditCommunication;
import com.nab.daf.common.security.CustomFilter;

@SpringBootApplication
@EntityScan("com.nab.daf.login.entity")
@EnableJpaRepositories(basePackages = "com.nab.daf.login.repository")
@ComponentScan(basePackages = "com.nab.daf", excludeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, value = {
		AuditCommunication.class, CustomFilter.class }))
public class LoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoginApplication.class, args);
	}

}
