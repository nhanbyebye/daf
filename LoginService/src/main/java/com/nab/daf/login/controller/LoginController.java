package com.nab.daf.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nab.daf.common.model.Response;
import com.nab.daf.login.model.AccountModel;
import com.nab.daf.login.service.LoginService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/v1/login", produces = "application/json")
@Api("Product Services - retrieve the product relalted information")
public class LoginController {

	@Autowired
	private LoginService loginService;

	@PostMapping(produces = "application/json")
	@ApiOperation("Authorize account")
	public Response<String> login(@RequestBody AccountModel accountModel) {
		return loginService.login(accountModel);
	}

}
