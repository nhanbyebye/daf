package com.nab.daf.login.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nab.daf.common.enums.StatusEnums;
import com.nab.daf.common.model.Response;
import com.nab.daf.common.utils.EncodeUtils;
import com.nab.daf.common.utils.TokenUtils;
import com.nab.daf.login.entity.Account;
import com.nab.daf.login.model.AccountModel;
import com.nab.daf.login.repository.AccountRepository;

@Service
public class LoginService {
	
	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AccountRepository accRepo;

	public Response<String> login(AccountModel accountModel) {
		Account account = accRepo.findByUsernameAndPassword(accountModel.getUsername(),
				EncodeUtils.encode(accountModel.getPassword()));
		if (account == null) {
			logger.info("Account not found: {}", StatusEnums.INVALID_CREDENTIAL.getMessage());
			return new Response<>(StatusEnums.INVALID_CREDENTIAL);
		}
		
		logger.info("Account Found");

		String jwtToken = TokenUtils.generateToken(account.getUsername(), account.getPassword());
		logger.debug("JWT Token generated: {}", jwtToken);
		return new Response<>(jwtToken);
	}

}
