package com.nab.daf.login.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountModel {
  private String username;
  private String password;
}
