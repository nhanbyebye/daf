# DAF

This file explains:
*  brief explanation for the code folder structure
*  all the required steps in order to get the application run on local computer
*  full CURL commands to verify the APIs (include full request endpoint, HTTP Headers and request payload if any)

## Project Overview
+ **AuditService**  : expose API to track user activity.
+ **common**        : include all shared functions/features.
+ **document**      : include HLD + ERD + Class Diagrams.
+ **LoginService**  : expose API to authenticate account.
+ **ProductService**: expose APIs to manage Product.
+ **db_scrips**     : db dump.

## Folder Structure
  - src/main/java:
      - com
        - nab
           - daf
              - {project-name}
                 - **MainApplication**: main class to start application.
                 - controller: expose rest APIs.
                 - service: business logic implemntation.
                 - entity: mapped with table in database.
                 - repository: list of CRUD operations.
                 - model: POJO classes.
                 - aop: log incoming request/response for @RestController using AOP
                 - communication: take care communication with other services or external system.
                 - config: configure application.
                 - constant: constant class.
                 - enums: enum class.
                 - security: authenticate incoming request.
                 - utils: utils class.
  - src/main/resource:
       - **application.yml**: spring boot configuration.
       - **logback.xml**: configure logging.
  - src/test/java: include list of test cases.
  
## INSTALLATION
1) [JDK 8](https://www.oracle.com/java/technologies/javase-jdk8-downloads.html)
2) [MySQL + Workbench](https://dev.mysql.com/downloads/installer/)
3) [Maven](https://maven.apache.org/download.cgi)

## DB SETUP
After install MySQL, open Workbench and run 2 script files under **db_script** folder: 
*  daf.sql
*  audit.sql

## APPLICATION CONFIGURATION
Edit src/main/resources/application.yml 

For ProductService and LoginService:

    spring:

        datasource:
      
            url: jdbc:mysql://<ip>:<port>/daf
            
            username: <username>
            
            password: <password>
    
    
For AuditService:

    spring:

        datasource:
  
            url: jdbc:mysql://<ip>:<port>/audit
            
            username: <username>
            
            password: <password>

## RUNNING
1) Build Common:
- Execute command in root project: `mvn clean install`

2) Build other project (ProductService, AuditService and LoginService):
- Execute command in root project: `mvn clean install`
- Execute command in /target folder to start applications:
    - LoginService: `java -jar login-service-0.0.1-SNAPSHOT.jar`
    - AuditService: `java -jar audit-service-0.0.1-SNAPSHOT.jar`
    - ProductService: `java -jar product-service-0.0.1-SNAPSHOT.jar`

## SWAGGER
Test APIs in Swagger UI. Also, curl commands can be found there:
- ProductService: [http://localhost:9000/swagger-ui.html](http://localhost:9002/swagger-ui.html)
- LoginService: [http://localhost:9001/swagger-ui.html](http://localhost:9002/swagger-ui.html)
- LoginService: [http://localhost:9002/swagger-ui.html](http://localhost:9002/swagger-ui.html)

## CURL COMMANDS
Due to security enabled, call /login api to get token and pass it to subsequence requests.

- LoginService: `curl -X POST "http://localhost:9002/v1/login" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"password\": \"admin\",  \"username\": \"admin\"}"`

- ProductService: Get returned token from LoginService and pass it to **X-Authorization** header
    
    - addProduct: `curl -X POST "http://localhost:9000/v1/product/add" -H  "accept: application/json" -H  "X-Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjoidXNlciIsImlhdCI6MTU4NDgzNTIwMCwiZXhwIjoxNTg1MDA4MDAwfQ.v9DwBKhSrCLpPuRxVF6rvkYKYZe45NJCd0kEDG6--9U" -H  "Content-Type: application/json" -d "{  \"brandId\": 1,  \"colorId\": 1,  \"name\": \"Galaxy Note 10\",  \"price\": 100}"`
    
    - updateProduct: `curl -X POST "http://localhost:9000/v1/product/update" -H  "accept: application/json" -H  "X-Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsInJvbGVzIjoidXNlciIsImlhdCI6MTU4NDgzNTIwMCwiZXhwIjoxNTg1MDA4MDAwfQ.v9DwBKhSrCLpPuRxVF6rvkYKYZe45NJCd0kEDG6--9U" -H  "Content-Type: application/json" -d "{  \"brandId\": 2,  \"colorId\": 2,  \"id\": 28,  \"name\": \"IPhone 9 - updated\",  \"price\": 123}"`
    
    - productDetail: `curl -X GET "http://localhost:9000/v1/product/id/28" -H  "accept: application/json"`
    
    - searchProduct: `curl -X POST "http://localhost:9000/v1/product/search?page=0&size=20&sort=id%2Cdesc" -H  "accept: application/json" -H  "Content-Type: application/json" -d "{  \"brandIds\": [    1, 2  ],  \"colorIds\": [    1, 2  ],  \"name\": \"\"}"`