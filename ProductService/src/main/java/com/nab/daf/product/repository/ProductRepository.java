package com.nab.daf.product.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nab.daf.product.entity.Product;
import com.nab.daf.product.entity.ProductBrand;
import com.nab.daf.product.entity.ProductColor;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("SELECT p FROM Product p WHERE (:name is null OR p.name LIKE :name%) "
			+ " AND (coalesce(:colors, null) is null OR p.color IN (:colors)) "
			+ " AND (coalesce(:brands, null) is null OR p.brand IN (:brands)) ")
	Page<Product> searchProduct(@Param("name") String name, @Param("colors") List<ProductColor> colors,
			@Param("brands") List<ProductBrand> brands, Pageable pageable);
}
