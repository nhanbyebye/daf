package com.nab.daf.product.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nab.daf.common.constant.Constant;
import com.nab.daf.common.enums.StatusEnums;
import com.nab.daf.common.model.Response;
import com.nab.daf.product.model.ProductCriteria;
import com.nab.daf.product.model.ProductModel;
import com.nab.daf.product.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/v1/product", produces = "application/json")
@Api("Product Services - retrieve the product relalted information")
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping(path = "/search", produces = "application/json")
	@ApiOperation("To retrieve product information based on criteria")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", dataType = "int", paramType = "query", defaultValue = "0", value = "Results page you want to retrieve (0..N)"),
			@ApiImplicitParam(name = "size", dataType = "int", paramType = "query", defaultValue = "20", value = "Number of records per page."),
			@ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "String", paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
					+ "Default sort order is ascending. " + "Multiple sort criteria are supported.") })
	public Response<Page<ProductModel>> searchProduct(@RequestBody ProductCriteria criteria, Pageable pageable) {
		Page<ProductModel> data = productService.searchProduct(criteria, pageable);
		return new Response<>(data);
	}

	@GetMapping(path = "/id/{id}", produces = "application/json")
	@ApiOperation("To retrieve product information based on criteria")
	public Response<ProductModel> retrieveProductDetail(@PathVariable Long id) {
		ProductModel productModel = productService.retrieveProductDetail(id);
		return productModel == null ? new Response<>(null, StatusEnums.PRODUCT_NOT_FOUND)
				: new Response<>(productModel);
	}

	@PostMapping(path = "/add", produces = "application/json")
	@ApiOperation("To add new product")
	public Response<Void> addProduct(
			@RequestHeader(name = Constant.AUTHORIZATION_HEADER, defaultValue = Constant.TOKEN_PREFIX) final String header,
			@RequestBody ProductModel productModel, HttpServletRequest httpRequest) {
		return new Response<>(productService.saveProduct(productModel, httpRequest));
	}

	@PostMapping(path = "/update", produces = "application/json")
	@ApiOperation("To update the existing product")
	public Response<Void> updateProduct(
			@RequestHeader(name = Constant.AUTHORIZATION_HEADER, defaultValue = Constant.TOKEN_PREFIX) final String header,
			@RequestBody ProductModel productModel, HttpServletRequest httpRequest) {
		return new Response<>(productService.updateProduct(productModel, httpRequest));
	}

	@PostMapping(path = "/delete", produces = "application/json")
	@ApiOperation("To delete the existing product")
	public Response<Void> deleteProduct(@RequestBody ProductModel productModel, HttpServletRequest httpRequest) {
		return new Response<>(productService.deleteProduct(productModel));
	}

}
