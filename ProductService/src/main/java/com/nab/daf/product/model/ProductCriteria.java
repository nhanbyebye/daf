package com.nab.daf.product.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCriteria {
  private String name;
  private List<Long> colorIds;
  private List<Long> brandIds;
}
