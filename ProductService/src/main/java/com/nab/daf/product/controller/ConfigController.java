package com.nab.daf.product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nab.daf.common.model.Response;
import com.nab.daf.product.model.ConfigValues;
import com.nab.daf.product.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(path = "/v1/config", produces = "application/json")
@Api("Config Services - retrieve the configuration values")
public class ConfigController {

  @Autowired
  private ProductService productService;

  @GetMapping
  @ApiOperation("To retrieve config information")
  public Response<ConfigValues> retrieveConfigValues() {
    return new Response<>(productService.retrieveLOVs());
  }

}
