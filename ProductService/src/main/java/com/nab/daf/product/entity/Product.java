package com.nab.daf.product.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Product extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "colorId", referencedColumnName = "id", nullable = false)
	private ProductColor color;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "brandId", referencedColumnName = "id", nullable = false)
	private ProductBrand brand;

	private String name;
	private Double price;

}
