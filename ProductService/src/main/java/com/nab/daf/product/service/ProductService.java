package com.nab.daf.product.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.nab.daf.common.constant.Constant;
import com.nab.daf.common.enums.StatusEnums;
import com.nab.daf.product.entity.Product;
import com.nab.daf.product.entity.ProductBrand;
import com.nab.daf.product.entity.ProductColor;
import com.nab.daf.product.model.ConfigValues;
import com.nab.daf.product.model.ProductCriteria;
import com.nab.daf.product.model.ProductModel;
import com.nab.daf.product.repository.ProductBrandRepository;
import com.nab.daf.product.repository.ProductColorRepository;
import com.nab.daf.product.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	private ProductColorRepository productColorRepo;

	@Autowired
	private ProductBrandRepository productBrandRepo;

	@Autowired
	private ProductRepository productRepo;

	public Page<ProductModel> searchProduct(ProductCriteria criteria, Pageable pageable) {
		List<ProductColor> colors = null;
		if (!CollectionUtils.isEmpty(criteria.getColorIds())) {
			colors = productColorRepo.findAllById(criteria.getColorIds());
			if (colors.isEmpty()) {
				return null;
			}
		}

		List<ProductBrand> brands = null;
		if (!CollectionUtils.isEmpty(criteria.getBrandIds())) {
			brands = productBrandRepo.findAllById(criteria.getBrandIds());
			if (brands.isEmpty()) {
				return null;
			}
		}

		Page<Product> productPage = productRepo.searchProduct(criteria.getName(), colors, brands, pageable);

		if (productPage.getTotalElements() == 0) {
			return null;
		}

		List<ProductModel> productModels = productPage.stream().map(p -> new ProductModel(p))
				.collect(Collectors.toList());

		return new PageImpl<>(productModels, productPage.getPageable(), productPage.getTotalElements());
	}

	public ConfigValues retrieveLOVs() {
		Map<Long, String> colorMap = productColorRepo.findAll().stream()
				.collect(Collectors.toMap(ProductColor::getId, ProductColor::getName));
		Map<Long, String> brandMap = productBrandRepo.findAll().stream()
				.collect(Collectors.toMap(ProductBrand::getId, ProductBrand::getName));

		return new ConfigValues(colorMap, brandMap);
	}

	public ProductModel retrieveProductDetail(Long productId) {
		Optional<Product> productOpt = productRepo.findById(productId);
		if (!productOpt.isPresent()) {
			return null;
		}

		return new ProductModel(productOpt.get());
	}

	public StatusEnums saveProduct(ProductModel productModel, HttpServletRequest httpRequest) {
		String username = httpRequest.getAttribute(Constant.USERNAME).toString();
		
		Product product = new Product();
		product.setCreatedDate(LocalDateTime.now());
		product.setCreatedBy(username);
		
		return saveProduct(productModel, httpRequest, product);
	}
	
	public StatusEnums saveProduct(ProductModel productModel, HttpServletRequest httpRequest, Product product) {
		Optional<ProductColor> colorOpt = productColorRepo.findById(productModel.getColorId());
		if (!colorOpt.isPresent()) {
			return StatusEnums.COLOR_NOT_FOUND;
		}

		Optional<ProductBrand> brandOpt = productBrandRepo.findById(productModel.getBrandId());
		if (!brandOpt.isPresent()) {
			return StatusEnums.BRAND_NOT_FOUND;
		}
		
		BeanUtils.copyProperties(productModel, product);
		product.setColor(colorOpt.get());
		product.setBrand(brandOpt.get());
		
		productRepo.save(product);
		return StatusEnums.SUCCESS;
	}

	public StatusEnums updateProduct(ProductModel productModel, HttpServletRequest httpRequest) {
		String username = httpRequest.getAttribute(Constant.USERNAME).toString();
		Optional<Product> productOpt = productRepo.findById(productModel.getId());

		if (!productOpt.isPresent()) {
			return StatusEnums.PRODUCT_NOT_FOUND;
		}
		
		Product product = productOpt.get();
		product.setUpdatedDate(LocalDateTime.now());
		product.setUpdatedBy(username);

		return saveProduct(productModel, httpRequest, product);
	}

	public StatusEnums deleteProduct(ProductModel productModel) {
		Optional<Product> productOpt = productRepo.findById(productModel.getId());

		if (!productOpt.isPresent()) {
			return StatusEnums.PRODUCT_NOT_FOUND;
		}

		productRepo.delete(productOpt.get());
		return StatusEnums.SUCCESS;
	}

}
