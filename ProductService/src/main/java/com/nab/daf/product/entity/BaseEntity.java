package com.nab.daf.product.entity;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@MappedSuperclass
public class BaseEntity {

  @CreatedDate
  private LocalDateTime createdDate;
  
  @CreatedBy
  private String createdBy;
  
  @LastModifiedDate
  private LocalDateTime updatedDate;
  
  @LastModifiedBy
  private String updatedBy;
}
