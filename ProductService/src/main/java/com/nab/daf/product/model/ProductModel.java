package com.nab.daf.product.model;

import org.springframework.beans.BeanUtils;

import com.nab.daf.product.entity.Product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductModel {
	private Long id;
	private Long colorId;
	private Long brandId;
	private String name;
	private Double price;

	public ProductModel() {
	}

	public ProductModel(Product product) {
		BeanUtils.copyProperties(product, this);
		this.colorId = product.getColor().getId();
		this.brandId = product.getBrand().getId();
	}
}
