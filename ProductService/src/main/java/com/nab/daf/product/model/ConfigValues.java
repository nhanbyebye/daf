package com.nab.daf.product.model;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ConfigValues {
  private Map<Long, String> colors;
  private Map<Long, String> brands;
}
