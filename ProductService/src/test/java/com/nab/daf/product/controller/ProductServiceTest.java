package com.nab.daf.product.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.util.CollectionUtils;

import com.nab.daf.product.entity.Product;
import com.nab.daf.product.entity.ProductBrand;
import com.nab.daf.product.entity.ProductColor;
import com.nab.daf.product.model.ConfigValues;
import com.nab.daf.product.model.ProductCriteria;
import com.nab.daf.product.model.ProductModel;
import com.nab.daf.product.repository.ProductBrandRepository;
import com.nab.daf.product.repository.ProductColorRepository;
import com.nab.daf.product.repository.ProductRepository;
import com.nab.daf.product.service.ProductService;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceTest {

	@Rule
	public MockitoRule mockitoRule = MockitoJUnit.rule();

	@InjectMocks
	private ProductService productService;

	@Mock
	private ProductColorRepository productColorRepo;

	@Mock
	private ProductBrandRepository productBrandRepo;

	@Mock
	private ProductRepository productRepo;
	
	private ProductCriteria criteria;
	private Pageable pageable;
	private ProductColor color;
	private ProductBrand brand;
	private Product prod;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		
		criteria = new ProductCriteria();
		criteria.setName("test");
		criteria.setBrandIds(Lists.list(1l));
		criteria.setColorIds(Lists.list(1l));
		
		pageable = PageRequest.of(0, 20);
		
		color = new ProductColor();
		color.setId(1l);
		color.setName("red");
		
		brand = new ProductBrand();
		brand.setId(1l);
		brand.setName("Apple");
		
		prod = new Product();
		prod.setId(1l);
		prod.setName("test");
		prod.setColor(color);
		prod.setBrand(brand);
		
	}

	@Test
	public void testSearchProduct() {
		Page<Product> productPage = new PageImpl<>(Lists.list(prod));
		
		when(productColorRepo.findAllById(criteria.getColorIds())).thenReturn(Lists.list(color));
		when(productBrandRepo.findAllById(criteria.getBrandIds())).thenReturn(Lists.list(brand));
		when(productRepo.searchProduct(any(), any(), any(), any())).thenReturn(productPage);
		
		Page<ProductModel> productModelPage = productService.searchProduct(criteria, pageable);
		
		assertEquals(productPage.get().findFirst().get().getName(), productModelPage.get().findFirst().get().getName());
	}
	
	@Test
	public void testSearchProduct_NotFound() {
		Page<Product> productPage = new PageImpl<>(new ArrayList<>());
		
		when(productColorRepo.findAllById(criteria.getColorIds())).thenReturn(Lists.list(color));
		when(productBrandRepo.findAllById(criteria.getBrandIds())).thenReturn(Lists.list(brand));
		when(productRepo.searchProduct(any(), any(), any(), any())).thenReturn(productPage);
		
		Page<ProductModel> productModelPage = productService.searchProduct(criteria, pageable);
		
		assertNull(productModelPage);
	}
	
	@Test
	public void testSearchProduct_InvalidColor() {
		when(productColorRepo.findAllById(criteria.getColorIds())).thenReturn(new ArrayList<>());
		
		Page<ProductModel> productModelPage = productService.searchProduct(criteria, pageable);
		
		assertNull(productModelPage);
	}
	
	@Test
	public void testSearchProduct_InvalidBrand() {
		when(productColorRepo.findAllById(criteria.getColorIds())).thenReturn(Lists.list(color));
		when(productBrandRepo.findAllById(criteria.getBrandIds())).thenReturn(new ArrayList<>());
		
		Page<ProductModel> productModelPage = productService.searchProduct(criteria, pageable);
		
		assertNull(productModelPage);
	}
	
	@Test
	public void testRetrieveLOVs() {
		when(productColorRepo.findAll()).thenReturn(Lists.list(color));
		when(productBrandRepo.findAll()).thenReturn(Lists.list(brand));
		
		ConfigValues configValues = productService.retrieveLOVs();
		
		assertTrue(configValues.getColors().containsKey(1l));
		assertEquals(color.getName(), configValues.getColors().get(1l));
		
		assertTrue(configValues.getBrands().containsKey(1l));
		assertEquals(brand.getName(), configValues.getBrands().get(1l));
	}
	
	@Test
	public void testRetrieveLOVs_Error() {
		when(productColorRepo.findAll()).thenReturn(new ArrayList<>());
		when(productBrandRepo.findAll()).thenReturn(new ArrayList<>());
		
		ConfigValues configValues = productService.retrieveLOVs();
		
		assertTrue(CollectionUtils.isEmpty(configValues.getColors()));
		assertTrue(CollectionUtils.isEmpty(configValues.getBrands()));
	}

}
