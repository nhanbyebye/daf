package com.nab.daf.common.constant;

import java.util.Arrays;
import java.util.List;

public class Constant {
	public static final String AUTHORIZATION_HEADER = "X-Authorization";
	public static final String MISSING_AUTHORIZATION_HEADER = "Missing " + AUTHORIZATION_HEADER + " header";
	public static final String INVALID_AUTHORIZATION_HEADER = "Invalid token. Please call '/login' API to get token";
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String SECRET_KEY = "DAF_SECRET_KEY";
	public static final String USERNAME = "username";

	public static final List<String> EXCLUDE_URIs = Arrays.asList("/v2/api-docs",
            "/configuration/ui",
            "/swagger-resources",
            "/configuration/security",
            "/swagger-ui.html",
            "/csrf",
            "/webjars", "/login", "/audit", "/product/search", "/product/id", "/config");

	private Constant() {
	}

}
