package com.nab.daf.common.model;

import java.io.Serializable;

import com.nab.daf.common.enums.StatusEnums;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseStatus implements Serializable {
  private static final long serialVersionUID = 1L;
  int code;
  String message;

  public ResponseStatus() {
  }

  public void setStatus(StatusEnums status) {
    this.code = status.getCode();
    this.message = status.getMessage();
  }

  public ResponseStatus(int code, String message) {
    this.code = code;
    this.message = message;
  }
}
