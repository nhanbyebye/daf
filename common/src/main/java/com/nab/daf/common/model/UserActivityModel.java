package com.nab.daf.common.model;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserActivityModel {
  private String ip;
  private String url;
  private String request;
  private String response;
  private long executionTime;
  private LocalDateTime accessTime;
}