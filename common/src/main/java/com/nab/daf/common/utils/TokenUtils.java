package com.nab.daf.common.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import com.nab.daf.common.constant.Constant;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenUtils {

	public static String generateToken(String username, String password) {
		LocalDateTime currentDate = LocalDate.now().atStartOfDay();
		LocalDateTime expireDate = currentDate.plusDays(2l);

		return Jwts.builder().setSubject(username).claim("roles", "user")
				.setIssuedAt(Date.from(currentDate.toInstant(ZoneOffset.UTC)))
				.setExpiration(Date.from(expireDate.toInstant(ZoneOffset.UTC)))
				.signWith(SignatureAlgorithm.HS256, Constant.SECRET_KEY).compact();
	}

	public static String getUsernameFromToken(String token) {
		if (token.startsWith(Constant.TOKEN_PREFIX)) {
			token = token.substring(7);
		}
		final Claims claims = Jwts.parser().setSigningKey(Constant.SECRET_KEY).parseClaimsJws(token).getBody();

		return claims.getSubject();
	}

	private TokenUtils() {
	}
}
