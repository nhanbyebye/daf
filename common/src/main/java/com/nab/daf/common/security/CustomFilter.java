package com.nab.daf.common.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.nab.daf.common.constant.Constant;
import com.nab.daf.common.utils.TokenUtils;

@Component
public class CustomFilter extends GenericFilterBean {

	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) res;
		String token = request.getHeader(Constant.AUTHORIZATION_HEADER);

		if ("OPTIONS".equals(request.getMethod()) || isExcludeURI(request.getRequestURI())) {
			response.setStatus(HttpServletResponse.SC_OK);

			chain.doFilter(req, res);
			return;
		}

		if (token == null) {
			throw new ServletException(Constant.MISSING_AUTHORIZATION_HEADER);
		}

		try {
			String username = TokenUtils.getUsernameFromToken(token);

			if (StringUtils.isEmpty(username)) {
				throw new ServletException(Constant.INVALID_AUTHORIZATION_HEADER);
			}

			req.setAttribute(Constant.USERNAME, username);

		} catch (Exception e) {
			throw new ServletException(Constant.INVALID_AUTHORIZATION_HEADER);
		}

		chain.doFilter(req, res);

	}

	public boolean isExcludeURI(String requestURI) {
		for (String uri : Constant.EXCLUDE_URIs) {
			if (requestURI.contains(uri))
				return true;
		}

		return false;
	}
}
