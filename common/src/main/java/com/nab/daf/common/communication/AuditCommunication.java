package com.nab.daf.common.communication;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.nab.daf.common.model.UserActivityModel;

import lombok.extern.slf4j.Slf4j;

@Component
public class AuditCommunication {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RestTemplate restTemplate;

	@Value("${services.audit.write-url}")
	private String auditUrl;

	@Async
	public void trackUserActivity(String ip, String url, String request, String response, long executionTime) {

		UserActivityModel userActivity = new UserActivityModel();
		userActivity.setAccessTime(LocalDateTime.now());
		userActivity.setExecutionTime(executionTime);
		userActivity.setIp(ip);
		userActivity.setRequest(request);
		userActivity.setResponse(response);
		userActivity.setUrl(url);
		
		try {
			restTemplate.postForEntity(auditUrl, userActivity, Void.class);
		} catch (RestClientException e) {
			logger.error("Invoke audit service occured exception: {}", e);
		}
	}
}
