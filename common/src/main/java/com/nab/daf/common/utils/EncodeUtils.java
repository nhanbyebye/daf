package com.nab.daf.common.utils;

import java.util.Base64;

public final class EncodeUtils {

	public static String encode(String s) {
		return Base64.getEncoder().encodeToString(s.getBytes());
	}

	public static String decode(String s) {
		return new String(Base64.getDecoder().decode(s));
	}

	private EncodeUtils() {
	}
}