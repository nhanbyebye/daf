package com.nab.daf.common.model;

import java.io.Serializable;

import com.nab.daf.common.enums.StatusEnums;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response<T> implements Serializable {
	private static final long serialVersionUID = 1L;
	T data;
	ResponseStatus status;

	public Response() {
		this.status = new ResponseStatus(StatusEnums.SUCCESS.getCode(), StatusEnums.SUCCESS.getMessage());
	}

	public Response(T data) {
		this.status = new ResponseStatus(StatusEnums.SUCCESS.getCode(), StatusEnums.SUCCESS.getMessage());
		this.data = data;
	}

	public Response(StatusEnums statusEnum) {
		this.status = new ResponseStatus(statusEnum.getCode(), statusEnum.getMessage());
	}

	public Response(T data, StatusEnums statusEnum) {
		this.data = data;
		this.status = new ResponseStatus(statusEnum.getCode(), statusEnum.getMessage());
	}

}