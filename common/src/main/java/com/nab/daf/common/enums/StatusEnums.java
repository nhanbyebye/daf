package com.nab.daf.common.enums;

import lombok.Getter;

@Getter
public enum StatusEnums {
  SUCCESS(0, "SUCCESS"), SERVER_ERROR(-1, "Server Error"), NOT_IMPLEMENTED(501, "Method Not Implemented Yet"),
  PRODUCT_NOT_FOUND(-10, "Product Id is not found"), COLOR_NOT_FOUND(-11, "Color is not found"), 
  BRAND_NOT_FOUND(-12, "Brand is not found"), INVALID_CREDENTIAL(-2, "Username/Password is not correct");

  private final int code;
  private final String message;

  private StatusEnums(int code, String message) {
    this.code = code;
    this.message = message;
  }

}