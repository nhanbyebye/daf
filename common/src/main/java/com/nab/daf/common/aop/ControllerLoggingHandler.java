package com.nab.daf.common.aop;

import java.lang.annotation.Annotation;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestBody;

import com.nab.daf.common.communication.AuditCommunication;
import com.nab.daf.common.utils.JSONUtils;

/**
 * Log incoming request and response for @RestController.
 * Moreover, store user activity to database.
 * */
@Aspect
@Component
public class ControllerLoggingHandler {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired(required = false)
	private AuditCommunication auditService;

	@Autowired(required = true)
	private HttpServletRequest httpRequest;

	@Around("within(@org.springframework.web.bind.annotation.RestController *)")
	public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

		String className = joinPoint.getSignature().getDeclaringTypeName();
		String methodName = joinPoint.getSignature().getName();

		Annotation[][] annotationMatrix = ((MethodSignature) joinPoint.getSignature()).getMethod()
				.getParameterAnnotations();
		String requestBody = null;

		StopWatch stopWatch = new StopWatch();
		logger.info("[START] {}.{}()", className, methodName);

		int index = 0;
		for (Annotation[] annotations : annotationMatrix) {
			for (Annotation annotation : annotations) {
				if (!(annotation instanceof RequestBody))
					continue;
				requestBody = JSONUtils.convertToJson(joinPoint.getArgs()[index]);
				logger.debug("Request body: {}", requestBody);
			}
			index++;
		}

		stopWatch.start();
		Object result = joinPoint.proceed();
		String response = JSONUtils.convertToJson(result);
		stopWatch.stop();
		long executionTime = stopWatch.getLastTaskTimeMillis();

		logger.debug("{}.{}() Response: {}", className, methodName, response);
		logger.debug("{}.{}() execution time: {} ms", className, methodName, executionTime);
		logger.info("[END] {}.{}()", className, methodName);

		if (auditService != null) {
			auditService.trackUserActivity(httpRequest.getRemoteAddr(), httpRequest.getRequestURI(), requestBody,
					response, executionTime);
		}

		return result;
	}

}
